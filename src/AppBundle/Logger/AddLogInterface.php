<?php

namespace AppBundle\Logger;


use AppBundle\Entity\RequestLog;

interface AddLogInterface
{
    public function add(RequestLog $log);
}