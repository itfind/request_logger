<?php
/**
 * Created by IntelliJ IDEA.
 * User: shev
 * Date: 11.11.17
 * Time: 17:49
 */

namespace AppBundle\Logger;


use AppBundle\Entity\RequestLog;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class DoctrineLogger implements AddLogInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DoctrineLogger constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function add(RequestLog $log)
    {
        $this->em->persist($log);
        $this->em->flush();
    }

}