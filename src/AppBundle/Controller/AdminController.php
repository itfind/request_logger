<?php

namespace AppBundle\Controller;

use AppBundle\Form\SearchHttpLogForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 * @package AppBundle\Controller
 */
class AdminController extends Controller
{

    /**
     * @Route("/admin/http-log")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function httpLogAction(Request $request)
    {

        $searchHttpForm = new SearchHttpLogForm();

        $form = $this->createFormBuilder($searchHttpForm)
            ->add('ip', TextType::class)
            ->add('search', SubmitType::class, array('label' => 'search'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $searchHttpForm = $form->getData();
        }

        $query = $this->getDoctrine()->getRepository('AppBundle:RequestLog')
            ->getQueryForSearchHttpLogForm($searchHttpForm);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('AppBundle:Admin:http_log.html.twig', array(
            'form' => $form->createView(),
            'pagination' => $pagination
        ));
    }

}
