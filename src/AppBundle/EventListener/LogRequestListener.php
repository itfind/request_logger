<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\RequestLog;
use AppBundle\Logger\AddLogInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;

class LogRequestListener
{
    private $logger;

    /**
     * LogRequestListener constructor.
     * @param $logger AddLogInterface
     */
    public function __construct($logger)
    {
        $this->logger = $logger;
    }


    public function onKernelTerminate(PostResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();
        If (!$request || !$response) {
            return;
        }
        if (!$request->query->get('logger', false)) {
            return;
        }

        $clientIp = $request->getClientIp();

        $requestLog = new RequestLog();
        $requestLog->setUrl($request->getUri());
        $requestLog->setHttpStatus($response->getStatusCode());
        $requestLog->setRequest((string)$request->headers);
        $requestLog->setResponse((string)$response->headers);
        $requestLog->setCreatedAt(new \DateTime());
        $requestLog->setClientIp($clientIp);

        $this->logger->add($requestLog);
    }
}