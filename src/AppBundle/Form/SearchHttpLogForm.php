<?php


namespace AppBundle\Form;


/**
 * Class SearchHttpLogForm
 * @package AppBundle\Form
 */
class SearchHttpLogForm
{
    /**
     * @var string
     */
    private  $ip;

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }
    
}